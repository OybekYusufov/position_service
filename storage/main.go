package storage

import (
	"bitbucket.org/Udevs/position_service/storage/postgres"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StorageI interface {
	Profession() repo.ProfessionRepoI
}

type storagePg struct {
	db         *sqlx.DB
	profession repo.ProfessionRepoI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		db:         db,
		profession: postgres.NewProfessionRepo(db),
	}
}

func (s *storagePg) Profession() repo.ProfessionRepoI {
	return s.profession
}
